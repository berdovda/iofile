﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IOFile
{
    class Program
    {
        static void Main(string[] args)
        {
            //Тестовый файл расположен в папке "Мои документы"
            var x = Calculations.GenerateSeq();
            Calculations.WriteDataToFile(x);
            Console.Write("Initially generated sequence: ");
            foreach (var i in x)
                Console.Write(i + ",");
            Console.WriteLine();
            x = Calculations.ReadDataFromFile();
            var a = x.OrderBy(i => i);
            var b = x.OrderByDescending(i => i);
            Console.Write("Sorted by ascending: ");
            foreach (var i in a)
                Console.Write(i + ",");
            Console.WriteLine();
            Console.Write("Sorted by descending: ");
            foreach (var i in b)
                Console.Write(i + ",");
        }
    }
}
