﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace IOFile
{
    public static class Calculations
    {
        public static List<int> GenerateSeq()
        {
            Random rand = new Random();
            List<int> possible = Enumerable.Range(0, 21).ToList();
            List<int> listNumbers = new List<int>();
            for (int i = 0; i < 21; i++)
            {
                int index = rand.Next(0, possible.Count);
                listNumbers.Add(possible[index]);
                possible.RemoveAt(index);
            }

            return listNumbers;
        }

        public static void WriteDataToFile(List<int> data)
        {
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string seq = "";
            foreach (int i in data)
                seq += i.ToString() + ",";
            seq = seq.Remove(seq.Length - 1);
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "WriteLines.txt")))
            {
                outputFile.Write(seq);
            }
        }

        public static List<int> ReadDataFromFile()
        {
            List<int> listNumbers = new List<int>();
            string seq = "";
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            using (StreamReader inputFile = new StreamReader(Path.Combine(docPath, "WriteLines.txt")))
            {
                while (inputFile.Peek() >= 0)
                {
                    seq = inputFile.ReadLine();
                }
            }
            return seq.Split(',').Select(Int32.Parse).ToList();
        }






    }
}

